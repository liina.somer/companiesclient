// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {

    // Function calls for initial page loading activities...
    doLoadCompanies();
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLoadCompanies() {
    console.log('Loading companies...');
    let companies = await fetchCompanies();
    displayCompanies(companies);
}

async function doDeleteCompany(companyId) {
    if (confirm('Soovid sa tõesti ettevõtet kustutada?')) {
        await removeCompany(companyId);
        await doLoadCompanies();
    }
}
/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayCompanies(companies) {
    const mainElement = document.querySelector('main');
    let companiesHtml = '';

    for(const company of companies){
        companiesHtml += /*html*/`
        <div class = "company-item-box">
        <div class="company-item-logo"><img src="${company.logo}" width="200"></div> 
        <div class = "company-item-property">Nimi: </div>
        <div class = "company-item-value">${company.name}</div>
        <div class = "company-item-property">Asutatud: </div>   
        <div class = "company-item-value">${company.established}</div> 
        <div class = "company-item-property">Töötajaid: </div>   
        <div class = "company-item-value">${company.employees}</div> 
        <div class="company-item-button">
        <button class="button" onClick="doDeleteCompany(${company.id})">Kustuta</button>
        <button onclick="displayCompanyEditPopup(${company.id})">Muuda</button>
        </div>
    </div>
        `;
    }
    mainElement.innerHTML = companiesHtml;
}


async function displayCompanyEditPopup(companyId) {
    await openPopup(POPUP_CONF_500_500, 'companyEditFormTemplate');

    if(companyId > 0) {
        //Muudame olemasolevat ettevõtet
        //täidame vormi väljad
        const company = await fetchCompany(companyId);
        document.querySelector('#companyId').value = company.id;
        document.querySelector('#companyName').value = company.name;
        document.querySelector('#companyLogo').value = company.logo;
        document.querySelector('#companyEstablished').value = company.established;
        document.querySelector('#companyEmployees').value = company.employees;
    }
}
async function editCompany() {
    const validationResult = validateCompanyForm();

    if (validationResult.length == 0){
    let company;
    if(document.querySelector('#companyId').value > 0) {
        //olemasoleva ettevõtte muutmine
    company = {
        id: document.querySelector('#companyId').value,
        name: document.querySelector('#companyName').value,
        logo: document.querySelector('#companyLogo').value,
        established: document.querySelector('#companyEstablished').value,
        employees: document.querySelector('#companyEmployees').value
    };
    await putCompany(company);
    } else {
    //uue ettevõtte lisamine
    company = {
        name: document.querySelector('#companyName').value,
        logo: document.querySelector('#companyLogo').value,
        established: document.querySelector('#companyEstablished').value,
        employees: document.querySelector('#companyEmployees').value

    };

    await postCompany(company);
    }
    await doLoadCompanies();
    await closePopup();
} else {
    displayCompanyFormErrors(validationResult);
}
}
function validateCompanyForm() {
    let errors = [];
    let companyName = document.querySelector('#companyName').value;
    let companyLogo = document.querySelector('#companyLogo').value;
    let companyEstablished = document.querySelector('#companyEstablished').value;
    let companyEmployees = document.querySelector('#companyEmployees').value;

    if(companyName.length < 2) {
        errors.push('Ettevõtte nimi kas määramata või liiga lühike');
    }
    if(companyLogo.length < 15) {
        errors.push('Ettevõtte logo määramata!')
    }
    if(companyLogo.length > 15 && !companyLogo.startsWith("http")) {
        errors.push('Ettevõtte logo peab algama kas http://...või https://...!')
    }
    if(companyEstablished == '') {
        errors.push('Ettevõtte asutamisaeg määramata')
    }
    if(companyEmployees < 1) {
        errors.push('Ettevõtte töötajate arv määramata')
    }
    return errors;
}
function displayCompanyFormErrors(errors) {
    const errorBox = document.querySelector('#errorBox');
    errorBox.style.display = 'block';

    let errorsHtml = '';
    for (let errorMessage of errors) {
        errorsHtml += /*html*/`<div>${errorMessage}</div>`;
    }
    errorBox.innerHTML = errorsHtml;
}

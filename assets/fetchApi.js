
async function fetchCompanies() {
    try {
        const response = await fetch(`${API_URL}/companies/all`);
        return await response.json();

      }  catch (e){
          //kui ming jama ilmneb ja promise ei resolvu
          console.log('Error occured', e);
      }
}

async function fetchCompany(companyId){
    try{
        const response = await fetch(`${API_URL}/companies/${companyId}`);
        return await response.json();
     } catch (e) {
        console.log('Error occurred', e);
     }
}

async function removeCompany(companyId) {
    try{
        const response = await fetch(`${API_URL}/companies/${companyId}`, { method: 'DELETE' });
        return await response.json();
     } catch (e) {
        console.log('Error occurred', e);
     }
     
}

async function postCompany(company) {
    try{
        const response = await fetch(`${API_URL}/companies/add`, { 
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(company) 
        });
        return await response.json();
     } catch (e) {
        console.log('Error in adding company', e);
     }
     
}
async function putCompany(company){
    try{
        const response = await fetch(`${API_URL}/companies/update`, { 
            method: 'PUT', 
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(company) 
        });
        return await response.json();
     } catch (e) {
        console.log('Error in modifying', e);
     }
}

